
import { describe, expect, test, beforeEach, vi, it } from "vitest"
import { shallowMount, mount } from "@vue/test-utils";
//components
import PokemonPage from "@/pages/PokemonPage.vue";
import { pokemons } from "../mocks/pokemons.mock";

describe('PokemonPage Component', () => {

  let wrapper
  beforeEach(() => {
    wrapper = shallowMount(PokemonPage)
  })

  test('debe de hacer match con el snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })

  test('debe de llamar mixPokemonArray al montar', () => {
    /* es muy tarde para observar ello ya q el cmp se ha ya montado en beforeEach
    const mixPokemonArraySpy = vi.spyOn(PokemonPage.methods, 'mixPokemonArray') */
    const mixPokemonArraySpy = vi.spyOn(PokemonPage.methods, 'mixPokemonArray')
    const wrapper = shallowMount(PokemonPage)


    expect(mixPokemonArraySpy).toHaveBeenCalled()
  })

  it('debe de hacer match con el snapshot cuando cargan los pokemons', () => {

    const wrapper = mount(PokemonPage, {
      data() {
        return {
          pokemonArr: pokemons,
          pokemon: pokemons[0],
          showPokemon: false,
          showAnswer: false,
          message: ''
        }
      }
    })

    expect(wrapper.html()).toMatchSnapshot()
  })

  it('debe de mostrar los componentes de PokemonPicture y PokemonOptions', () => {

    const wrapper = shallowMount(PokemonPage, {
      data() {
        return {
          pokemonArr: pokemons,
          pokemon: pokemons[0],
          showPokemon: false,
          showAnswer: false,
          message: ''
        }
      }
    })

    //pokemonpicture debe de existir
    //pokemonoptions debe de existir
    expect(wrapper.find('pokemon-picture-stub').exists()).toBeTruthy()
    expect(wrapper.find('pokemon-options-stub').exists()).toBeTruthy()
    //pokemonpicture attribute pokemonid === '1'
    expect(wrapper.find('pokemon-picture-stub').attributes('pokemonid')).toBe('1')
    expect(wrapper.find('pokemon-options-stub').attributes('pokemons')).toBeTruthy()

    //pokemonoptions attribute pokemons tobe true
  })

  it('pruebas con checkanswer', async () => {

    const wrapper = shallowMount(PokemonPage, {
      data() {
        return {
          pokemonArr: pokemons,
          pokemon: pokemons[0],
          showPokemon: false,
          showAnswer: false,
          message: ''
        }
      }
    })

    await wrapper.vm.checkAnswer(1)

    expect(wrapper.find('h2').exists()).toBeTruthy()
    expect(wrapper.vm.showPokemon).toBeTruthy()
    expect ( wrapper.find ('h2'). text () ). toBe(`Correcto, ${ pokemons[0].name }`)

    await wrapper.vm.checkAnswer(10)
    expect ( wrapper.vm.message ). toBe(`Oops, era ${ pokemons[0].name }`)

  })
  
})