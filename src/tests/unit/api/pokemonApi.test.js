
import pokemonApi from "@/api/pokemonApi"
import { describe, expect, test } from "vitest"

describe('pokemoApi', () => {
  test('axios debe estar configurado con el api de pokemon', () => {
    expect(pokemonApi.defaults.baseURL).toBe('https://pokeapi.co/api/v2/pokemon')
  })
})