/**
 * El spying sirve para conatrolar una función sobre cuándo ha sido llamada sin cambiar el comportamiento original
 * vi: abreviatura para vitest
 */

import { test, expect, describe, vi } from "vitest";

const greeting = (name: string) => {
  console.log('Hello, ' + name);
}

describe('Mocking & Spying in Vitest', () => {
  /**
   * Aunqy vemos el log en consola, no tenemos la informacion para comprobarlo
   * So, para probar eso podríamos espiar las funciones console.log
   */
  test('Greeting', () => {
    // expect(greeting('World')).toBe(undefined)
    const spy = vi.spyOn(console, 'log')

    greeting('World')
    greeting('Anthony')

    expect(spy).toBeCalledTimes(2)
    //primer indice indica el orden, el 2do indica la posicion del argumento
    /* expect(spy).toBeCalledWith('Hello, World')
    expect(spy).toBeCalledWith('Hello, Anthony') */
    expect(spy.mock.calls[0][0]).toBe('Hello, World')
    expect(spy.mock.calls[1][0]).toBe('Hello, Anthony')
  })

  test('Greeting  2', () => {
    const spy = vi.spyOn(console, 'log')

    greeting('World')
    expect(spy).toBeCalledTimes(1)
    expect(spy).toBeCalledWith('Hello, World')

    spy.mockReset()

    greeting('Anthony')
    expect(spy).toBeCalledTimes(1)
    expect(spy).toBeCalledWith('Hello, Anthony')

    expect(spy).toMatchInlineSnapshot(`
      [MockFunction log] {
        "calls": [
          [
            "Hello, Anthony",
          ],
        ],
        "results": [
          {
            "type": "return",
            "value": undefined,
          },
        ],
      }
    `)
  })

})