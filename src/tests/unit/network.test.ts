
import { test, expect, vi, beforeAll, afterAll } from 'vitest'
import { getPostBody } from '../network'
/* vi.stubGlobal('fetch', async () => {
  return {
    json() {
      return {
        body: 'foo'
      }
    }
  }
}) */
/**
 * Añadiendo beforeAll y afterAll de vitest, permitirá a msw comenzar a interceptar las peticiones
 */


test('should fetch', async () => {
  const result = await getPostBody(1)

  expect(result).toMatchInlineSnapshot('"Mocked for 1!"')
  
  /* expect(result).toMatchInlineSnapshot(`
    "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
  `) */
})