/**
 * En algunos escenarios más complejos, tu código podría depender en alguna otra
función de otros módulos, o paquetes npm.
 */


import { test, expect, vi } from 'vitest'
import { loadConfig } from '../fs'

/**
 * el primer argumento es el nomnbre o la ruta al modulo y el sgdo argumento una funcion d fabrica para crear el modulo fs
 */

/* vi.mock('fs', async () => {
  const actual = await vi.importActual('fs')
  return {
    ...actual,
    // default: {
      readFileSync: () => '{ "name": "from fs" }'
    // }
  }
}) */

vi.mock('fs', async (importOriginal) => {
  const actual = await importOriginal() as typeof import('fs')
  return {
    ...actual,
    readFileSync: () => '{ "name": "from fs" }'
  }
})

test('with fs', async () => {
  const result = await loadConfig()
  expect(result).toEqual({ name: 'from fs' })
})