
import { test, describe, expect } from "vitest";

const deepMerge = (a, b) => {
  if(Array.isArray(a) && Array.isArray(b))
    return [...a, ...b]
  // return Object.assign(a,b)
  if (Array.isArray(a) || Array.isArray(b) || typeof a !== typeof b) {
    throw new Error('Can not merge two different types')
  }
  const merged = { ...a }
  for (const key of Object.keys(b)) {
    if(typeof a[key] === 'object' || Array.isArray(a[key]))
      merged[key] = deepMerge(a[key], b[key]) 
    else
      merged[key] = b[key]
  }
  return merged
}

describe('Suite', () => {

  test('Shallow merge with overlaps', () => {
    const merged = deepMerge(
      {
        name: 'Eric',
        github: 'unknown',
      },
      {
        github: 'ericvipo',
        twitter: 'ericvipo'
      }
    )

    expect(merged).toEqual({
      name: 'Eric',
      github: 'ericvipo',
      twitter: 'ericvipo'
    })

  })

  test('Shallow merge with arrays', () => {
    const merged = deepMerge(
      ['vue', 'react'],
      ['svelte', 'solid']
    )

    expect(merged).toEqual([
      'vue', 'react', 'svelte', 'solid'
    ])

  })

  test('Deep merge with overlaps', () => {
    const merged = deepMerge(
      {
        name: 'Eric',
        accounts: {
          github: 'unknown'
        },
        languages: ['javascript']
      },
      {
        accounts: {
          twitter: 'ericvipo'
        },
        languages: ['typescript', 'vue']
      }
    )

    /* expect(merged).toEqual({
      name: 'Eric',
      accounts: {
        github: 'unknown',
        twitter: 'ericvipo'
      }
    }) */

    // expect(merged).toMatchSnapshot()
    expect(merged).toMatchInlineSnapshot(`
    {
      "accounts": {
        "github": "unknown",
        "twitter": "ericvipo",
      },
      "languages": [
        "javascript",
        "typescript",
        "vue",
      ],
      "name": "Eric",
    }
    `)

  })

  //aprenderemos cómo probar los errores
  /* test.fails('Throws errors on merging two different types', () => {
    // const merged = deepMerge(
    //   ['foo', 'bar'],
    //   { foo: 'bar' }
    // )
  }) */

  test('Throws errors on merging two different types', () => {
    /* const merged = deepMerge(
      ['foo', 'bar'],
      { foo: 'bar' }
    )
    expect(merged).toThrowError() */

    expect(() => deepMerge(
      ['foo', 'bar'],
      { foo: 'bar' }
    )).toThrowError('Can not merge two different types')
  })

})