
import { describe, expect, test, beforeEach } from "vitest"
import { shallowMount } from "@vue/test-utils";
//components
import PokemonOptions from "@/components/PokemonOptions.vue";

import { pokemons } from "../mocks/pokemons.mock";

describe('PokemonOptions Component', () => {

  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(PokemonOptions, {
      props: {
        pokemons
      }
    })
  })
  
  test('debe de hacer match con el snapshot', () => {
    // console.log(wrapper.html());
    expect(wrapper.html()).toMatchInlineSnapshot(`
      "<div data-v-39189cb9="" class="options-container">
        <ul data-v-39189cb9="">
          <li data-v-39189cb9="">bulbasaur</li>
          <li data-v-39189cb9="">ivysaur</li>
          <li data-v-39189cb9="">venusaur</li>
          <li data-v-39189cb9="">charmander</li>
        </ul>
      </div>"
    `)

  })

  test('debe de mostrar las 4 opciones correctamente', () => {
    const liTags = wrapper.findAll('li')
    expect(liTags.length).toBe(4)

    expect(liTags[0].text()).toBe('bulbasaur')

  })

  test('debe de emitir "selection" con sus respectivos parámetros al hacer click', () => {
    const [li1, li2, li3, li4] = wrapper.findAll('li')
    li1.trigger('click')//estimulo
    li2.trigger('click')//estimulo
    li3.trigger('click')//estimulo
    li4.trigger('click')//estimulo
    console.log(wrapper.emitted('selection'));
    expect(wrapper.emitted('selection').length).toBe(4)//asercion
    expect(wrapper.emitted('selection')[0]).toEqual([1])//asercion
  })

})