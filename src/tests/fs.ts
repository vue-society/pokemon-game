import { readFileSync, existsSync } from 'fs'

export function loadConfig() {
  if(!existsSync('./src/tests/config.json'))
    return undefined
  return JSON.parse(readFileSync('./src/tests/config.json', 'utf-8'))
}