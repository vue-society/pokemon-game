
import { beforeAll, afterAll } from 'vitest'
import { setupServer } from "msw/node";
import { http, HttpResponse } from "msw";

const server = setupServer(
  http.get(
    "https://jsonplaceholder.typicode.com/posts/:id",
    ({params}) => {
      const {id} = params
      return HttpResponse.json({
        body: `Mocked for ${id}!`
      })
    }
  )
)
beforeAll(() => server.listen())
afterAll(() => server.close())